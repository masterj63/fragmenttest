package ru.samsu.ibaryshnikov.fragmenttest.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import ru.samsu.ibaryshnikov.fragmenttest.fragment.TabFragmentPlace;
import ru.samsu.ibaryshnikov.fragmenttest.fragment.TabFragmentTime;

public class PagerAdapter extends FragmentPagerAdapter {
    final int NUM_TAB;

    public PagerAdapter(FragmentManager fm, int numTab) {
        super(fm);
        this.NUM_TAB = numTab;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new TabFragmentPlace();
            case 1:
                return new TabFragmentTime();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return NUM_TAB;
    }
}